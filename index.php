<?php
    require_once("animal.php");
    require_once("ape.php");
    require_once("frog.php");

    $sheep = new Animal("Shaun");
    echo "Name : ".$sheep->name."<br>";
    echo "legs : ".$sheep->legs."<br>";
    echo "Cold Blooded : ".$sheep->cold_blood."<br><br>";

    $frog = new Frog("Buduk");
    echo "Name : ".$frog->name."<br>";
    echo "legs : ".$frog->legs."<br>";
    echo "Cold Blooded : ".$frog->cold_blood."<br>";
    echo $frog->jump()."<br><br>";

    $ape = new Ape("Kera Sakti");
    echo "Name : ".$ape->name."<br>";
    echo "legs : ".$ape->legs."<br>";
    echo "Cold Blooded : ".$ape->cold_blood."<br>";
    echo $ape->yell();
?>